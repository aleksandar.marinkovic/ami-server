const express = require("express");
const routes = express.Router();
const cors = require("cors");

// This will help us connect to the database
const dbo = require("../db/conn");

// This section will help you get a list of all the categories.
routes.route("/categories").get(function (request, response) {
  let db_connect = dbo.getDb();
  db_connect
    .collection("categories")
    .find({})
    .toArray()
    .then((result) => {
      response.json(result);
    });
});

// This section will help you get a single category by id
routes.route("/categories/:id").get(function (request, response) {
  let db_connect = dbo.getDb();
  let myquery = { _id: parseInt(request.params.id) };
  db_connect
    .collection("categories")
    .findOne(myquery)
    .then((result) => {
      response.json(result);
    });
});

// This section will help you get all quotes of a single category by id
routes.route("/categories/:id/quotes").get(function (request, response) {
  let db_connect = dbo.getDb();
  let myquery = { category_id: parseInt(request.params.id) };
  db_connect
    .collection("quotes")
    .find(myquery)
    .toArray()
    .then((result) => {
      response.json(result);
    });
});

// This section will help you get a list of all the quotes.
routes.route("/quotes").get(function (request, response) {
  let db_connect = dbo.getDb();
  db_connect
    .collection("quotes")
    .find({})
    .toArray()
    .then((result) => {
      response.json(result);
    });
});

// This section will help you get a list of all the quotes from one or more categories.
routes.route("/quotes").post(function (request, response) {
  let db_connect = dbo.getDb();
  let myquery = {};
  if (Object.keys(request.body.category_id).length > 0) {
    myquery =
      request.body.category_id.length > 1
        ? { category_id: { $in: request.body.category_id }, isApproved: true }
        : { category_id: parseInt(request.body.category_id), isApproved: true };
  }
  db_connect
    .collection("quotes")
    .find(myquery)
    .toArray()
    .then((result) => {
      response.json(result);
    });
});

// This section will help you get a quote
routes.route("/quotes/:id").get((request, response) => {
  let db_connect = dbo.getDb();
  let myquery = { quote_id: parseInt(request.params.id) };
  db_connect
    .collection("quotes")
    .findOne(myquery)
    .then((result) => {
      response.json(result);
    });
});

// This section will help you create a new quote.
routes.route("/quotes/add").post(function (request, response) {
  let db_connect = dbo.getDb();
  let myobj = {
    text: request.body.text,
    author: request.body.author,
    date: request.body.date,
    category_id: request.body.category_id,
    isApproved: false,
  };
  db_connect
    .collection("quotes")
    .insertOne(myobj)
    .then((result) => {
      response.json(result);
    });
});

module.exports = routes;
