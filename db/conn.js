const { MongoClient, ServerApiVersion } = require("mongodb");
const Db = process.env.MONGO_URI;
const client = new MongoClient(Db, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  serverApi: ServerApiVersion.v1,
});

let _db;

module.exports = {
  connectToServer: async function (callback) {
    try {
      await client.connect();
    } catch (error) {
      console.error(error);
    }
    // Verify we got a good "db" object
    _db = client.db("QuotesDB");
    console.log("Successfully connected to MongoDB.");
    return _db === undefined ? false : true;
  },

  getDb: function () {
    return _db;
  },
};
